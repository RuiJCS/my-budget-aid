# init db inside container
docker exec -it pg_container bash
psql -h pg_container -d finances -U root -f init_db.sql


# Create user
curl -X POST http://localhost:8000/user -d '{"name": "rui"}' -H 'content-type: application/json'

# Create movement for user
curl -v -X POST http://localhost:8000/user/1/movement -d '{"expense": 1, "value": 5.25, "description": "I was hungry"}' -H 'content-type: application/json'

# Get users
curl -v -X GET http://localhost:8000/user/

# Get Movements for users
curl -v -X GET http://localhost:8000/user/1/movement/