use warp::{Reply, reply::json, reject, hyper::StatusCode, Rejection};

use crate::{DBPool, Result, model, db::user, db::movement};

pub async fn list_movements_handler(user_id: i32, db_pool: DBPool) -> Result<impl Reply> {
    let movements = match movement::fetch(&db_pool, user_id)
            .await
            .map_err(reject::custom) {
                Ok(movement) => movement,
                Err(error) => {
                    if error.is_not_found() {
                        let error_value: Option<&Rejection>  = error.find();
                        if error_value.is_some() {
                            println!("{:?}", error_value.unwrap());
                        }
                    };
                    let mut res: Vec<model::Movement> = Vec::new();
                    res.push(model::Movement{
                        id: -1,
                        user_id: user_id,
                        expense: 0,
                        value: 0f32,
                        description: String::from("create failed")
                    });
                    res
                },
            };
    Ok(json::<Vec<_>>(
        &movements.into_iter().map(model::MovementResponse::of).collect(),
    ))
}

pub async fn create_movement_handler(
    user_id: i32,
    body: model::MovementRequest,
    db_pool: DBPool,
) -> Result<impl Reply> {
    Ok(json(&model::MovementResponse::of(
        match movement::create(&db_pool, user_id, body)
                    .await
                    .map_err(reject::custom) {
                        Ok(movement) => movement,
                        Err(error) => {
                            if error.is_not_found() {
                                let error_value: Option<&Rejection>  = error.find();
                                if error_value.is_some() {
                                    println!("{:?}", error_value.unwrap());
                                }
                            }; 
                            model::Movement{
                                id: -1,
                                user_id: user_id,
                                expense: 0,
                                value: 0f32,
                                description: String::from("create failed")
                            }},
                    },
    )))
}

pub async fn delete_movement_handler(user_id: i32, id: i32, db_pool: DBPool) -> Result<impl Reply> {
    match movement::delete(&db_pool, user_id, id)
            .await
            .map_err(reject::custom) {
                Ok(movement) => movement,
                Err(error) => {
                    if error.is_not_found() {
                        let error_value: Option<&Rejection>  = error.find();
                        if error_value.is_some() {
                            println!("{:?}", error_value.unwrap());
                        }
                    }; 
                    0u64},
            };
    Ok(StatusCode::OK)
}

pub async fn list_users_handler(db_pool: DBPool) -> Result<impl Reply> {
    let users = match user::fetch(&db_pool).await.map_err(reject::custom) {
        Ok(movement) => movement,
        Err(error) => {
            if error.is_not_found() {
                let error_value: Option<&Rejection>  = error.find();
                if error_value.is_some() {
                    println!("{:?}", error_value.unwrap());
                }
            };
            let mut res: Vec<model::User> = Vec::new();
            res.push(
                model::User{
                    id: -1,
                    name: String::from("User not found"),
                    balance: 0f32,
                }
            );
            res},
    };
    Ok(json::<Vec<_>>(
        &users.into_iter().map(model::UserResponse::of).collect(),
    ))
}

pub async fn fetch_user_handler(id: i32, db_pool: DBPool) -> Result<impl Reply> {
    let user = match user::fetch_one(&db_pool, id)
            .await
            .map_err(reject::custom) {
                Ok(user) => {user},
                Err(error) => {
                    if error.is_not_found() {
                        let error_value: Option<&Rejection> = error.find();
                        if error_value.is_some() {
                            println!("{:?}", error_value.unwrap());
                        }
                    }; 
                    model::User{
                        id: id,
                        name: String::from("User not found"),
                        balance: 0f32,
                    }},
            };
    Ok(json(&model::UserResponse::of(user)))
}

pub async fn create_user_handler(body: model::UserRequest, db_pool: DBPool) -> Result<impl Reply> {
    Ok(json(&model::UserResponse::of(
        match user::create(&db_pool, body)
                    .await
                    .map_err(reject::custom) {
                        Ok(user) => {user},
                        Err(error) => {
                            if error.is_not_found() {
                                let error_value: Option<&Rejection> = error.find();
                                if error_value.is_some() {
                                    println!("{:?}", error_value.unwrap());
                                }
                            }; 
                            model::User{
                                id: -1,
                                name: String::from("User not created"),
                                balance: 0f32,
                            }},
            },
    )))
}