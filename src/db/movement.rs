use mobc_postgres::tokio_postgres::Row;

use crate::{model::{Movement, MovementRequest}, DBPool, db::{get_db_con, Result}, error::Error::*};

use super::user;

pub const TABLE: &str = "movement";
const SELECT_FIELDS: &str = "user_id, expense, value, description";

pub async fn fetch(db_pool: &DBPool, user_id: i32) -> Result<Vec<Movement>> {
    let con = get_db_con(db_pool).await?;
    let query = format!(
        "SELECT * FROM {} WHERE user_id = $1",
        TABLE
    );
    let rows = con
        .query(query.as_str(), &[&user_id])
        .await
        .map_err(DBQueryError)?;

    Ok(rows.iter().map(|r| row_to_movement(&r)).collect())
}

pub async fn create(db_pool: &DBPool, user_id: i32, body: MovementRequest) -> Result<Movement> {
    let con = get_db_con(db_pool).await?;
    let query = format!(
        "INSERT INTO {} ({}) VALUES ($1, $2, $3, $4) RETURNING *",
        TABLE,
        SELECT_FIELDS
    );
    let row = con
        .query_one(
            query.as_str(),
            &[&user_id, &body.expense, &body.value, &body.description],
        )
        .await
        .map_err(DBQueryError)?;
    Ok(row_to_movement(&row))
}

pub async fn delete(db_pool: &DBPool, user_id: i32, id: i32) -> Result<u64> {
    let con = get_db_con(db_pool).await?;
    let query = format!("DELETE FROM {} WHERE id = $1 AND user_id = $2", TABLE);
    con.execute(query.as_str(), &[&id, &user_id])
        .await
        .map_err(DBQueryError)
}

fn row_to_movement(row: &Row) -> Movement {
    let id: i32 = row.get(0);
    let user_id: i32 = row.get(1);
    let expense: i32 = row.get(2);
    let value: f32 = row.get(3);
    let description: String = row.get(4);
    Movement {
        id,
        user_id,
        expense,
        value,
        description,
    }
}