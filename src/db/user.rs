use mobc_postgres::tokio_postgres::Row;

use crate::{model::{User, UserRequest}, DBPool, db::{get_db_con, Result}, error::Error::*};

pub const TABLE: &str = "users";
const SELECT_FIELDS: &str = "id, name, balance";

pub async fn fetch(db_pool: &DBPool) -> Result<Vec<User>> {
    let con = get_db_con(db_pool).await?;
    let query = format!("SELECT {} FROM {}", SELECT_FIELDS, TABLE);
    let rows = con.query(query.as_str(), &[]).await.map_err(DBQueryError)?;

    Ok(rows.iter().map(|r| row_to_user(&r)).collect())
}

pub async fn fetch_one(db_pool: &DBPool, id: i32) -> Result<User> {
    let con = get_db_con(db_pool).await?;
    let query = format!("SELECT {} FROM {} WHERE id = $1", SELECT_FIELDS, TABLE);

    let row = con
        .query_one(query.as_str(), &[&id])
        .await
        .map_err(DBQueryError)?;
    Ok(row_to_user(&row))
}

pub async fn create(db_pool: &DBPool, body: UserRequest) -> Result<User> {
    let con = get_db_con(db_pool).await?;
    let query = format!("INSERT INTO {}(name) VALUES ($1) RETURNING *", TABLE);
    println!("{} {}", body.name,query);
    let row = con
        .query_one(query.as_str(), &[&body.name])
        .await
        .map_err(DBQueryError)?;
    Ok(row_to_user(&row))
}

fn row_to_user(row: &Row) -> User {
    let id: i32 = row.get(0);
    let name: String = row.get(1);
    let balance: f32 = row.get(2);
    User { id, name, balance }
}