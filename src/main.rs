use std::convert::Infallible;

use mobc::{Connection, Pool};
use mobc_postgres::{tokio_postgres, PgConnectionManager};
use tokio_postgres::NoTls;
use warp::{
    http::{header, Method},
    Filter, Rejection,
};

mod db;
mod error;
mod model;
mod handler;

type DBCon = Connection<PgConnectionManager<NoTls>>;
type DBPool = Pool<PgConnectionManager<NoTls>>;
type Result<T> = std::result::Result<T, Rejection>;

#[tokio::main]
async fn main() {
    let db_pool = db::create_pool().expect("database pool can be created");

    let movement = warp::path!("user" / i32 / "movement");
    let movement_param = warp::path!("user" / i32 / "movement" / i32);
    let user = warp::path("user");

    let movement_routes = movement
        .and(warp::get())
        .and(with_db(db_pool.clone()))
        .and_then(handler::list_movements_handler)
        .or(movement
            .and(warp::post())
            .and(warp::body::json())
            .and(with_db(db_pool.clone()))
            .and_then(handler::create_movement_handler))
        .or(movement_param
            .and(warp::delete())
            .and(with_db(db_pool.clone()))
            .and_then(handler::delete_movement_handler));

    let user_routes = user
        .and(warp::get())
        .and(warp::path::param())
        .and(with_db(db_pool.clone()))
        .and_then(handler::fetch_user_handler)
        .or(user
            .and(warp::get())
            .and(with_db(db_pool.clone()))
            .and_then(handler::list_users_handler))
        .or(user
            .and(warp::post())
            .and(warp::body::json())
            .and(with_db(db_pool.clone()))
            .and_then(handler::create_user_handler));

    let routes = movement_routes
        .or(user_routes)
        .recover(error::handle_rejection)
        .with(
            warp::cors()
                .allow_credentials(true)
                .allow_methods(&[
                    Method::OPTIONS,
                    Method::GET,
                    Method::POST,
                    Method::DELETE,
                    Method::PUT,
                ])
                .allow_headers(vec![header::CONTENT_TYPE, header::ACCEPT])
                .expose_headers(vec![header::LINK])
                .max_age(300)
                .allow_any_origin(),
        );

    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}

fn with_db(db_pool: DBPool) -> impl Filter<Extract = (DBPool,), Error = Infallible> + Clone {
    warp::any().map(move || db_pool.clone())
}