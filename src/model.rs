use serde::{Deserialize,Serialize};

#[derive(Deserialize, Clone, PartialEq, Debug)]
pub struct User {
    pub id: i32,
    pub name: String,
    pub balance: f32,
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct UserRequest {
    pub name: String
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct UserResponse {
    pub id: i32,
    pub name: String,
    pub balance: f32,
}


impl UserResponse {
    pub fn of(user: User) -> UserResponse {
        UserResponse {
            id: user.id,
            name: user.name,
            balance: user.balance,
        }
    }
}

#[derive(Deserialize, Clone, PartialEq, Debug)]
pub struct Movement {
    pub id: i32,
    pub user_id: i32,
    pub expense: i32,
    pub value: f32,
    pub description: String
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct MovementRequest {
    pub expense: i32,
    pub value: f32,
    pub description: String
}

#[derive(Serialize, Deserialize, Clone, PartialEq, Debug)]
pub struct MovementResponse {
    pub id: i32,
    pub expense: i32,
    pub value: f32,
    pub description: String
}

impl MovementResponse {
    pub fn of(movement: Movement) -> MovementResponse {
        MovementResponse {
            id: movement.id,
            expense: movement.expense,
            value: movement.value,
            description: movement.description
        }
    }
}